﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolarPal
{

    /// <summary>
    /// Params stores all the header data for the submitted polar file. Each valus a string.
    /// </summary>


    public static class Params
    {

        //Each value can be used to determine various settings relevant to the polar data. for example; the version of polar data collection the user used.
        public static string Version { get;  set; }
        public static string Monitor { get;  set; }
        public static string SModeString { get;  set; }
        public static string Date { get;  set; }
        public static string StartTime { get;  set; }
        public static string Length { get;  set; }
        public static string Interval { get;  set; }
        public static string Upper1 { get;  set; }
        public static string Lower1 { get;  set; }
        public static string Upper2 { get;  set; }
        public static string Lower2 { get;  set; }
        public static string Upper3 { get;  set; }
        public static string Lower3 { get;  set; }
        public static string Timer1 { get;  set; }
        public static string Timer2 { get;  set; }
        public static string Timer3 { get;  set; }
        public static string ActiveLimit { get;  set; }
        public static string MaxHR { get;  set; }
        public static string RestHR { get;  set; }
        public static string StartDelay { get;  set; }
        public static string VO2max { get;  set; }
        public static string Weight { get;  set; }

    }
}
