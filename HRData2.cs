﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace PolarPal
{

    ///<summary>
    /// HRData class handles the organisation of the major file data. It's objects are to be used when accessing the total data arrays.
    ///</summary>
    ///<remarks>
    ///do not replace the contents of these arrays as they are used as the main arrays for calculations. Instead create temporary arrays in the relevant classes.
    /// </remarks>



    public static class HRData2
    {
        /// <summary>
        /// main arrays for data.
        /// </summary>
        public static double[]
            HR,
            HRPercent,
            Speed,
            PowerPercent,
            Cadence,
            Altitude,
            Power,
            PPedalIndex;

        public static int[]
            pedalindex;
        public static string[]
            powerbalance;

        private static List<double> PowerPercentList = new List<double>();
        private static List<double> HeartRatePercentList = new List<double>();


        private static List<string> Powerbalance = new List<string>();
        private static List<int> pedalindexlist = new List<int>();

        public static int[]
            Intervals;

        public static DateTime
            StartTime,
            StartDate;

        /// <summary>
        /// Average data. displayed on main summary tab.
        /// </summary>
        public static double
            TotalDistance,
            MaxSpeed,
            MaxHR, 
            MaxPower, 
            MaxAltitude, 
            MinHR, 
            AverageSpeed, 
            AverageHR, 
            AveragePower, 
            AverageAlt;

        public static List<double> PowerPercentList1 { get => PowerPercentList; set => PowerPercentList = value; }
        public static List<double> HeartRatePercentList1 { get => HeartRatePercentList; set => HeartRatePercentList = value; }
        public static List<string> Powerbalance1 { get => Powerbalance; set => Powerbalance = value; }
        public static List<int> Pedalindexlist { get => pedalindexlist; set => pedalindexlist = value; }



        /// <summary>
        /// This method converts the entire array of speed objects to the european standard. i.e. kph
        /// </summary>
        public static void ConvertToEuro()
        {
            for (int i = 0; i < Speed.Length; i++)
            {

                Speed[i] = Math.Round(Speed[i] * 1.60934, 1);

            }
        }
        /// <summary>
        /// This method converts the entire array of speed units to the American standard measurement. i.e mph.
        /// </summary>
        public static void ConvertToUs()
        {
            for (int i = 0; i < Speed.Length; i++)
            {

                Speed[i] = Math.Round(Speed[i] * 0.621371, 1);

            }
        }

        /// <summary>
        /// The setsummarydata method contains various blocks of logic which determine the values for the average data.
        /// </summary>
        public static void SetSummaryData()
        {
            //set the pedal index and power balance data.
            GetbinaryIndex();

            //get the start date of the selected file.
            char[] time_arr = Params2.Date.ToCharArray();
            //set the startyear, month and day variables.
            string startyear = time_arr[0] +""+ time_arr[1] +""+ time_arr[2] +""+ time_arr[3];
            string startmonth = time_arr[4] + "" + time_arr[5];
            string startday = time_arr[6] + "" + time_arr[7];

            //parse the start date and time to date and time objects.
            StartDate = DateTime.Parse(startyear+"/"+startmonth+"/"+startday);
            StartTime = Convert.ToDateTime(Params2.StartTime);
            //Calculate the total distance of the session
            TotalDistance = Math.Round((Math.Round(Average(Speed), 2)) * TimeToDouble(Params2.Length), 2);

            //get the max and min heart rate.
            MaxHR = HR.Max();
            MinHR = HR.Min();
            //Get the average heart rate.
            AverageHR = Math.Round(Average(HR), 2);

            //If the SMOde power was on get the average and max power wattage of the session.
            if (SMode2.Power == true)
            {
                AveragePower = Math.Round(Average(Power), 2);
                MaxPower = Power.Max();
            }
            else
            {
                AveragePower = 0;
                MaxPower = 0;
            }

            //If the SMode speed was selected the get the max and average speed depending on which SMode measurement unit is selected.
            if (SMode2.Speed == true)
            {
                if (SMode2.UsEuro == true) // US
                {
                    MaxSpeed = Speed.Max();
                    AverageSpeed = Math.Round(Average(Speed), 2);
                }
                else // false = //Euro
                {
                    MaxSpeed = Speed.Max();
                    AverageSpeed = Math.Round(Average(Speed), 2);
                }
            }
            else
            {
                AverageSpeed = 0;
                MaxSpeed = 0;
            }

            //If the Smode altitude was selected get the max and average altitude from the session.
            if (SMode2.Altitude == true)
            {
                if (SMode2.UsEuro == true) // US
                {
                    MaxAltitude = Altitude.Max();
                    AverageAlt = Math.Round(Average(Altitude), 2);
                }
                else // false = //Euro
                {
                    MaxAltitude = Altitude.Max();
                    AverageAlt = Math.Round(Average(Altitude), 2);
                }
            }
            else
            {
                AverageAlt = 0;
                MaxAltitude = 0;
            }





        }

        /// <summary>
        /// Time to double will convert a date string to it's equivelant double value.
        /// </summary>
        /// <param name="str"> mustbe a string value: hh:mm</param>
        /// <returns>Returns a double value of the time string sent to it.</returns>
        public static double TimeToDouble(string str)
        {
            

            string[] str_arr = str.Split(':');

            string hours = str_arr[0];
            string minutes = str_arr[1];
            double.TryParse(hours, out double hour);
            double.TryParse(minutes, out double minute);
            double.TryParse(hours+"."+minutes, out double TimeToDouble);

            
            return TimeToDouble;
        }

        /// <summary>
        /// Finds the average value of a given double array.
        /// </summary>
        /// <param name="array"></param>
        /// <returns>a double array.</returns>
        public static double Average(params double[] array)
        {
            double sum = Sum(array);
            double result = (double)sum / array.Length;
            return result;
        }
        /// <summary>
        /// This method will return the sum of an array of double values.
        /// </summary>
        /// <param name="array"></param>
        /// <returns>Returns a double array</returns>
        private static double Sum(double[] array)
        {
            double result = 0;

            for (int i = 0; i < array.Length; i++)
            {
                result += array[i];
            }

            return result;
        }
        /// <summary>
        /// Converts the power array into a percentage of FTP (A field entered by the user)
        /// </summary>
        /// <param name="textboxtext"> textbox text must be a string but is converted in the logic.</param>
        public static void ConvertPower(string textboxtext)
        {
            PowerPercentList.Clear();
            double ParsePut;
            if(double.TryParse(textboxtext, out ParsePut))
            {
                

                for (int i = 0; i < Power.Length; i++)
                {

                    PowerPercentList.Add(Math.Round((Power[i] / ParsePut) * 100, 1));
                    

                }

                PowerPercent = PowerPercentList.ToArray();
                
            }
            else
            {
                MessageBox.Show("The value entered was not a number!!!!!");
            }
        }
        /// <summary>
        /// Converts the heart rate values to percentage of users maximum heart rate
        /// </summary>
        /// <param name="text">Max heart rate is entered in a text box and parsed as a number in this method.</param>
        public static void ConvertRate(string text)
        {
            HeartRatePercentList.Clear();

            double ParsePut;
            if (double.TryParse(text, out ParsePut))
            {


                for (int i = 0; i < HR.Length; i++)
                {

                    HeartRatePercentList.Add(Math.Round((HR[i] / ParsePut) * 100, 1));


                }

                HRPercent = HeartRatePercentList.ToArray();
                
            }
            else
            {
                MessageBox.Show("The value entered was not a number11111");
            }
        }
        /// <summary>
        /// The GetBinaryIndex method will convert the ppedalindex array from HRData and use masking to seperate the two values. The left lef power balance and the pedal index.
        /// </summary>
        internal static void GetbinaryIndex()
        {
            if (SMode.PPedalIndex)
            {

                byte index = new byte();
                for (int i = 0; i < PPedalIndex.Length; i++)
                {
                    int PowerB = (int)PPedalIndex[i];
                    byte left = (byte)(PowerB & 0xFFu); // lower 8 bits 
                    index = (byte)((PowerB >> 8) & 0xFFu); // top 8 bits

                    int indexint = 0;
                    int.TryParse(index.ToString(), out indexint);
                    int right = 100 - left;
                    Powerbalance1.Add(left.ToString() + "/" + right.ToString());
                    Pedalindexlist.Add(indexint);



                }

                powerbalance = Powerbalance1.ToArray();
                pedalindex = Pedalindexlist.ToArray();

            }
        }
    }






}
