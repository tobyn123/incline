﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace PolarPal
{

    /// <summary>
    /// The main class and dashboard windows form used by the user. This class contains all the objects for the application for all event driven features.
    /// </summary>


    public partial class Form1 : Form
    {
        //Set all the global variables used in the form.--------------------------------------------
        private string pathToFile;
        public static bool
            FTP,
            MaxHR,
            SecondFile;

        private static int NP1;

        double IF;

        //point pair lists for the graph.
        private PointPairList hr = new PointPairList();
        private PointPairList Speed = new PointPairList();
        private PointPairList Cadence = new PointPairList();
        private PointPairList Altitude = new PointPairList();
        private PointPairList Power = new PointPairList();
        private PointPairList PPedalIndex = new PointPairList();
        private PointPairList hr2 = new PointPairList();
        private PointPairList Speed2 = new PointPairList();
        private PointPairList Cadence2 = new PointPairList();
        private PointPairList Altitude2 = new PointPairList();
        private PointPairList Power2 = new PointPairList();
        private PointPairList PPedalIndex2 = new PointPairList();

        public static double[]
            HRlist,
            HRPercentlist,
            Speedlist,
            PowerPercentlist,
            Cadencelist,
            Altitudelist,
            Powerlist,
            PPedalIndexlist,
            HRlist1,
            HRPercentlist1,
            Speedlist1,
            PowerPercentlist1,
            Cadencelist1,
            Altitudelist1,
            Powerlist1,
            PPedalIndexlist1;

        public static double
            TotalDistance,
            MaxSpeed,
            AMaxHR,
            MaxPower,
            MaxAltitude,
            MinHR,
            AverageSpeed,
            AverageHR,
            AveragePower,
            AverageAlt,
            TotalDistance1,
            MaxSpeed1,
            AMaxHR1,
            MaxPower1,
            MaxAltitude1,
            MinHR1,
            AverageSpeed1,
            AverageHR1,
            AveragePower1,
            AverageAlt1;

        private YAxis y0, y1, y2, y3, y4 = null;

        public int NP2 { get; private set; }
        public double IF2 { get; private set; }
        //--------------------------------------

        /// <summary>
        /// Updates the user portion comparison data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (int.TryParse(textBox4.Text, out int boxint))
            {
                Console.WriteLine("nice");

                if (SMode2.Set)
                {
                    DataGridViewColumn col12 = new DataGridViewTextBoxColumn();
                    col12.HeaderText = "Heart Rate";
                    col12.Name = "HR1";
                    int colIndex16 = dataGridView4.Columns.Add(col12);



                    DataGridViewColumn col33 = new DataGridViewTextBoxColumn();
                    col33.HeaderText = "Heart Rate 2";
                    col33.Name = "HR2";
                    int colIndex34 = dataGridView4.Columns.Add(col33);

                    DataGridViewColumn col = new DataGridViewTextBoxColumn();
                    col.HeaderText = "Speed";
                    col.Name = "S1";
                    int colIndex1 = dataGridView4.Columns.Add(col);



                    DataGridViewColumn col3 = new DataGridViewTextBoxColumn();
                    col3.HeaderText = "Speed 2";
                    col3.Name = "S2";
                    int colIndex3 = dataGridView4.Columns.Add(col3);

                    DataGridViewColumn col4 = new DataGridViewTextBoxColumn();
                    col4.HeaderText = "Power";
                    col4.Name = "P1";
                    int colIndex4 = dataGridView4.Columns.Add(col4);


                    DataGridViewColumn col6 = new DataGridViewTextBoxColumn();
                    col6.HeaderText = "Power 2";
                    col6.Name = "P2";
                    int colIndex6 = dataGridView4.Columns.Add(col6);

                    DataGridViewColumn col7 = new DataGridViewTextBoxColumn();
                    col7.HeaderText = "Cadence";
                    col7.Name = "C1";
                    int colIndex7 = dataGridView4.Columns.Add(col7);



                    DataGridViewColumn col9 = new DataGridViewTextBoxColumn();
                    col9.HeaderText = "Cadence 2";
                    col9.Name = "C2";
                    int colIndex9 = dataGridView4.Columns.Add(col9);

                    DataGridViewColumn col10 = new DataGridViewTextBoxColumn();
                    col10.HeaderText = "Altitude";
                    col10.Name = "A11";
                    int colIndex10 = dataGridView4.Columns.Add(col10);



                    DataGridViewColumn col14 = new DataGridViewTextBoxColumn();
                    col14.HeaderText = "Altitude 2";
                    col14.Name = "A2";
                    int colIndex13 = dataGridView4.Columns.Add(col14);

                    for (int i = 0; i < boxint; i++)
                    {
                        //Create the new row first and get the index of the new row
                        int rowIndex = this.dataGridView4.Rows.Add();
                        var row = this.dataGridView4.Rows[rowIndex];

                        int hrnumber = HRlist.Count() / boxint;
                        int hrnumber1 = HRlist1.Count() / boxint;
                        List<double> tempSpeedlist = new List<double>();
                        List<double> tempHRlist = new List<double>();
                        List<double> temppowerlist = new List<double>();
                        List<double> tempaltlist = new List<double>();
                        List<double> tempcadencelist = new List<double>();

                        List<double> tempSpeedlist1 = new List<double>();
                        List<double> tempHRlist1 = new List<double>();
                        List<double> temppowerlist1 = new List<double>();
                        List<double> tempaltlist1 = new List<double>();
                        List<double> tempcadencelist1 = new List<double>();

                        for (int ii = 0; ii < hrnumber; ii++)
                        {

                            
                        }

                        for (int ii = 0; ii < hrnumber1; ii++)
                        {
                            //Create the new row first and get the index of the new row
                            rowIndex = this.dataGridView4.Rows.Add();

                            //Obtain a reference to the newly created DataGridViewRow 
                            row = this.dataGridView4.Rows[rowIndex];

                            row.Cells["HR1"].Value = HRlist[hrnumber * i + ii] + "bpm";
                            //row.Cells["hrpm"].Value = HRlist[hrnumber * i + ii] - HRlist1[hrnumber1 * i + ii - 1];
                            row.Cells["HR2"].Value = HRlist1[hrnumber1 * i + ii] + "bpm";

                            row.Cells["S1"].Value = Speedlist[hrnumber * i + ii] + "" + SMode.SpeedMetric;
                            //row.Cells["spm"].Value = Speedlist[hrnumber * i + ii] - Speedlist1[hrnumber1 * i + ii];
                            row.Cells["S2"].Value = Speedlist1[hrnumber1 * i + ii] + "" + SMode.SpeedMetric;

                            row.Cells["P1"].Value = Powerlist[hrnumber * i + ii] + "w";
                            //row.Cells["ppm"].Value = Powerlist[hrnumber * i + ii] - Powerlist1[hrnumber1 * i + ii];
                            row.Cells["P2"].Value = Powerlist1[hrnumber1 * i + ii] + "w";

                            row.Cells["C1"].Value = Cadencelist[hrnumber * i + ii];
                            //row.Cells["cpm"].Value = Cadencelist[hrnumber * i + ii] - Cadencelist1[hrnumber1 * i + ii];
                            row.Cells["C2"].Value = Cadencelist1[hrnumber1 * i + ii];

                            row.Cells["A11"].Value = Altitudelist[hrnumber * i + ii];
                            //row.Cells["apm"].Value = Altitudelist[hrnumber * i + ii] - Altitudelist1[hrnumber1 * i + ii];
                            row.Cells["A2"].Value = Altitudelist1[hrnumber1 * i + ii];
                        }


                        this.dataGridView4.Rows.Add();

                    }



                }
            }


        }

        /// <summary>
        /// Updates the comparison data for one file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click_1(object sender, EventArgs e)
        {
            if (int.TryParse(textBox3.Text, out int boxint))
            {

                DataGridViewColumn col = new DataGridViewTextBoxColumn();
                col.HeaderText = "Total Distance";
                col.Name = "TD";
                int colIndex1 = dataGridView3.Columns.Add(col);

                DataGridViewColumn col2 = new DataGridViewTextBoxColumn();
                col2.HeaderText = "Max Speed";
                col2.Name = "MS";
                int colIndex2 = dataGridView3.Columns.Add(col2);

                DataGridViewColumn col3 = new DataGridViewTextBoxColumn();
                col3.HeaderText = "Max Heart Rate";
                col3.Name = "MHR";
                int colIndex3 = dataGridView3.Columns.Add(col3);

                DataGridViewColumn col4 = new DataGridViewTextBoxColumn();
                col4.HeaderText = "Max Power";
                col4.Name = "MP";
                int colIndex4 = dataGridView3.Columns.Add(col4);

                DataGridViewColumn col5 = new DataGridViewTextBoxColumn();
                col5.HeaderText = "Max Altitude";
                col5.Name = "MA";
                int colIndex5 = dataGridView3.Columns.Add(col5);

                DataGridViewColumn col6 = new DataGridViewTextBoxColumn();
                col6.HeaderText = "Average Speed";
                col6.Name = "AS";
                int colIndex6 = dataGridView3.Columns.Add(col6);

                DataGridViewColumn col7 = new DataGridViewTextBoxColumn();
                col7.HeaderText = "Average Heart Rate";
                col7.Name = "AHR";
                int colIndex7 = dataGridView3.Columns.Add(col7);

                DataGridViewColumn col8 = new DataGridViewTextBoxColumn();
                col8.HeaderText = "Min Heart Rate";
                col8.Name = "MinHR";
                int colIndex8 = dataGridView3.Columns.Add(col8);

                DataGridViewColumn col9 = new DataGridViewTextBoxColumn();
                col9.HeaderText = "Average Power";
                col9.Name = "AP";
                int colIndex9 = dataGridView3.Columns.Add(col9);

                DataGridViewColumn col10 = new DataGridViewTextBoxColumn();
                col10.HeaderText = "Average Altitude";
                col10.Name = "AA";
                int colIndex10 = dataGridView3.Columns.Add(col10);

                for (int i = 0; i < boxint; i++)
                {
                    //Create the new row first and get the index of the new row
                    int rowIndex = this.dataGridView3.Rows.Add();

                    //Obtain a reference to the newly created DataGridViewRow 
                    var row = this.dataGridView3.Rows[rowIndex];

                    int hrnumber = HRlist.Count() / boxint;

                    int speednumber = Speedlist.Count() / boxint;

                    List<double> tempSpeedlist = new List<double>();
                    List<double> tempHRlist = new List<double>();
                    List<double> temppowerlist = new List<double>();
                    List<double> tempaltlist = new List<double>();

                    for (int ii = 0; ii < speednumber; ii++)
                    {
                        tempSpeedlist.Add(Speedlist[speednumber * i+ii]);
                        tempHRlist.Add(HRlist[hrnumber * i + ii]);
                        temppowerlist.Add(Powerlist[hrnumber * i + ii]);
                        tempaltlist.Add(Powerlist[hrnumber * i + ii]);
                    }
                    
                    double[] tempspeedarr = tempSpeedlist.ToArray();

                    double temptd = Math.Round(Math.Round(tempspeedarr.Average()) * (HRData.TimeToDouble(Params.Length) / boxint) , 2);
                    
                    row.Cells["TD"].Value = temptd + "" + SMode.SpeedMetric;
                    row.Cells["MS"].Value = tempspeedarr.Max() + "" + SMode.SpeedMetric;
                    row.Cells["MHR"].Value = tempHRlist.ToArray().Max() + "bpm";
                    row.Cells["MP"].Value = temppowerlist.ToArray().Max() + "w";
                    row.Cells["MA"].Value = tempaltlist.ToArray().Max() + "";
                    row.Cells["AS"].Value = Math.Round(tempspeedarr.Average(), 2) + "w";
                    row.Cells["AHR"].Value = Math.Round(tempHRlist.Average(), 2) + "bpm";
                    row.Cells["MinHR"].Value = Math.Round(tempHRlist.Min(), 2) + "bpm";
                    row.Cells["AP"].Value = Math.Round(temppowerlist.Average(), 2) + "w";
                    row.Cells["AA"].Value = Math.Round(tempaltlist.Average(), 2) + "";

                    
                }
            }
            else
            {
            }
        }
        /// <summary>
        /// Updates the comparison data for one file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (int.TryParse(textBox3.Text, out int boxint))
            {

                DataGridViewColumn col = new DataGridViewTextBoxColumn();
                col.HeaderText = "Total Distance";
                col.Name = "TD";
                int colIndex1 = dataGridView3.Columns.Add(col);

                DataGridViewColumn col2 = new DataGridViewTextBoxColumn();
                col2.HeaderText = "Max Speed";
                col2.Name = "MS";
                int colIndex2 = dataGridView3.Columns.Add(col2);

                DataGridViewColumn col3 = new DataGridViewTextBoxColumn();
                col.HeaderText = "Max Heart Rate";
                col.Name = "MHR";
                int colIndex3 = dataGridView3.Columns.Add(col3);

                DataGridViewColumn col4 = new DataGridViewTextBoxColumn();
                col.HeaderText = "Max Power";
                col.Name = "MP";
                int colIndex4 = dataGridView3.Columns.Add(col4);

                DataGridViewColumn col5 = new DataGridViewTextBoxColumn();
                col.HeaderText = "Max Altitude";
                col.Name = "MA";
                int colIndex5 = dataGridView3.Columns.Add(col5);

                DataGridViewColumn col6 = new DataGridViewTextBoxColumn();
                col.HeaderText = "Average Speed";
                col.Name = "AS";
                int colIndex6 = dataGridView3.Columns.Add(col6);

                DataGridViewColumn col7 = new DataGridViewTextBoxColumn();
                col.HeaderText = "Average Heart Rate";
                col.Name = "AHR";
                int colIndex7 = dataGridView3.Columns.Add(col7);

                DataGridViewColumn col8 = new DataGridViewTextBoxColumn();
                col.HeaderText = "Min Heart Rate";
                col.Name = "MinHR";
                int colIndex8 = dataGridView3.Columns.Add(col8);

                DataGridViewColumn col9 = new DataGridViewTextBoxColumn();
                col.HeaderText = "Average Power";
                col.Name = "AP";
                int colIndex9 = dataGridView3.Columns.Add(col9);

                DataGridViewColumn col10 = new DataGridViewTextBoxColumn();
                col.HeaderText = "Average Altitude";
                col.Name = "AA";
                int colIndex10 = dataGridView3.Columns.Add(col10);

                for (int i = 0; i < boxint; i++)
                {
                    //Create the new row first and get the index of the new row
                    int rowIndex = this.dataGridView3.Rows.Add();

                    //Obtain a reference to the newly created DataGridViewRow 
                    var row = this.dataGridView3.Rows[rowIndex];

                    int number = HRlist.Count() / boxint;
                    
                        row.Cells["HR"].Value = HRlist[i];


                    Console.WriteLine(HRlist.Count());
                    Console.WriteLine(number);

                }
            }
            else
            {
                Console.WriteLine("not a number");
            }
        }


        /// <summary>
        /// Initialises the main components and sets main window settings.
        /// </summary>
        public Form1()
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            InitializeComponent();
            Dictionary<string, string> comboSource = new Dictionary<string, string>();
            comboSource.Add("1", "Mph");
            comboSource.Add("2", "Kph");
            comboBox1.DataSource = new BindingSource(comboSource, null);
            comboBox1.DisplayMember = "Value";
            comboBox1.ValueMember = "Key";
            Dictionary<string, string> combo2source = new Dictionary<string, string>();
            combo2source.Add("1", "%");
            combo2source.Add("2", "Total");
            comboBox2.DataSource = new BindingSource(combo2source, null);
            comboBox2.DisplayMember = "Value";
            comboBox2.ValueMember = "Key";

            comboBox2.Enabled = false;




        }

        /// <summary>
        /// Clears the chart on the page of any curves.
        /// </summary>
        public void clearChart1()
        {
            zedGraphControl1.GraphPane.CurveList.Clear();
            zedGraphControl1.GraphPane.GraphObjList.Clear();
            zedGraphControl1.AxisChange();
            zedGraphControl1.Invalidate();

        }

        /// <summary>
        /// Clears the entire display for refresh.
        /// </summary>
        public void clearDisplay()
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            dataGridView1.Refresh();
            dataGridView2.Rows.Clear();
            dataGridView2.Columns.Clear();
            dataGridView2.Refresh();
        }

        /// <summary>
        /// Calculates the noramlised power for the session.
        /// </summary>
        /// <param name="isfirst"> Boolean to determine if the second file is the one being tested for normalised power.</param>
        public void calc_norm_power(bool isfirst = true)
        {

            if (isfirst)
            {
                List<int> initialPower1 = new List<int>();
                List<double> list1 = new List<double>();
                double total1 = 1;
                double average1;
                for (int ii = 1; ii < dataGridView1.RowCount; ii++)
                {
                    if (ii > dataGridView1.RowCount - 30)
                    {

                        if (list1.Count > 0)
                        {
                            average1 = list1.Average();
                            NP1 = Convert.ToInt32(Math.Pow(average1, 1.0 / 4.00));
                            label37.Text = "0 watts";
                            label37.Visible = true;
                        }

                    }
                    else
                    {
                        for (int n = 0; n < 30; n++)
                        {
                            string split = Powerlist[ii + n].ToString();
                            initialPower1.Add(Convert.ToInt32(split));
                        }
                        average1 = initialPower1.Average();
                        total1 = Math.Pow(average1, 4.00);
                        list1.Add(total1);
                        initialPower1.Clear();
                        average1 = list1.Average();
                        NP1 = Convert.ToInt32(Math.Pow(average1, 1.0 / 4.00));
                    }

                }
            }

            if (SMode2.Set)
            {
                List<int> initialPower2 = new List<int>();
                List<double> list2 = new List<double>();
                double total2 = 1;
                double average2;
                for (int ii = 1; ii < dataGridView2.RowCount; ii++)
                {
                    if (ii > dataGridView2.RowCount - 30)
                    {

                        if (list2.Count > 0)
                        {
                            average2 = list2.Average();
                            NP2 = Convert.ToInt32(Math.Pow(average2, 1.0 / 4.00));
                            label38.Text = NP2.ToString();
                            label38.Visible = true;
                        }

                    }
                    else
                    {
                        for (int n = 0; n < 29; n++)
                        {
                            string split = Powerlist1[ii + n].ToString();
                            initialPower2.Add(Convert.ToInt32(split));
                        }
                        average2 = initialPower2.Average();
                        total2 = Math.Pow(average2, 4.00);
                        list2.Add(total2);
                        initialPower2.Clear();
                        average2 = list2.Average();
                        NP2 = Convert.ToInt32(Math.Pow(average2, 1.0 / 4.00));
                        label38.Text = NP2.ToString();
                        label38.Visible = true;
                    }

                }
            }
        }

        /// <summary>
        /// Data load is the main method for loading the data from the hrm file class and preparing it for display.
        /// </summary>
        /// <param name="pathToFile">absolute path of the file.</param>
        /// <param name="secondFile">boolean determines whether second file is being processed for comparison.</param>
        public void dataLoad(string pathToFile, bool secondFile)
        {
            FTP = false;
            MaxHR = false;
            //clearChart1();
            //clearDisplay();
            hrmFile file = new hrmFile();

            file.Init(pathToFile, SecondFile);

            if (SecondFile)
            {
                if (SMode2.Set == true)
                {
                    HRlist1 = HRData2.HR;
                   
                    Speedlist1 = HRData2.Speed;
                    PowerPercentlist1 = HRData2.PowerPercent;
                    Cadencelist1 = HRData2.Cadence;
                    Altitudelist1 = HRData2.Altitude;
                    Powerlist1 = HRData2.Power;
                    PPedalIndexlist1 = HRData2.PPedalIndex;
                    HRData2.SetSummaryData();
                    dataGridView2.CellBorderStyle = DataGridViewCellBorderStyle.None;
                    dataGridView2.RowHeadersVisible = false;
                    UpdateDisplay2();
                    UpdateChart2();
                }
                else
                {
                }
            }
            else
            {
                if (SMode.Set == true && SMode2.Set != true)
                {

                    HRlist = HRData.HR;
                    
                    Speedlist = HRData.Speed;
                    PowerPercentlist = HRData.PowerPercent;
                    Cadencelist = HRData.Cadence;
                    Altitudelist = HRData.Altitude;
                    Powerlist = HRData.Power;
                    PPedalIndexlist = HRData.PPedalIndex;
                    HRData.SetSummaryData();
                    dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.None;
                    dataGridView1.RowHeadersVisible = false;
                    UpdateDisplay();
                    UpdateChart();

                }
                else
                {
                }
            }




        }

        /// <summary>
        /// Updates the main display components. such as the main datagridviews and the summary data.
        /// </summary>
        private void UpdateDisplay2()
        {

            SetAverageData();


            label13.Visible = true;
            label13.Text = HRData2.StartDate.ToString("dd/mm/yyyy") + "/" + HRData2.StartTime.ToString("hh:mm:ss");
            label13.Text += " - D: " + Params2.Length + " - I: " + Params2.Interval + "s";

            if (SMode2.UsEuro == false)
            {
                label35.Visible = true;
                label35.Text = (HRData2.TotalDistance) + "km";
                label34.Visible = true;
                label34.Text = (HRData2.MaxSpeed) + "km";
                label30.Visible = true;
                label30.Text = (HRData2.AverageSpeed) + "km";
            }
            else
            {
                label35.Visible = true;
                label35.Text = (HRData2.TotalDistance) + "mi";
                label34.Visible = true;
                label34.Text = (HRData2.MaxSpeed) + "mi";
                label30.Visible = true;
                label30.Text = (HRData2.AverageSpeed) + "mi";

            }

            label33.Visible = true;
            label32.Visible = true;
            label31.Visible = true;

            label33.Text = (HRData2.MaxHR) + "bpm";
            label32.Text = (HRData2.MaxPower) + "W";
            label31.Text = (HRData2.MaxAltitude) + "m";


            label29.Visible = true;
            label28.Visible = true;
            label27.Visible = true;
            label26.Visible = true;
            label29.Text = (HRData2.AverageHR) + "bpm";
            label28.Text = (HRData2.MinHR) + "bpm";
            label27.Text = (HRData2.AveragePower) + "bpm";
            label26.Text = (HRData2.AverageAlt) + "m";



            if (MaxHR)
            {
                DataGridViewColumn col = new DataGridViewTextBoxColumn();
                col.HeaderText = "% of Max HR";
                col.Name = "HR";
                int colIndex = dataGridView2.Columns.Add(col);
            }
            else
            {
                DataGridViewColumn col = new DataGridViewTextBoxColumn();
                col.HeaderText = "Heart Rate (BPM)";
                col.Name = "HR";
                int colIndex = dataGridView2.Columns.Add(col);
            }
            if (SMode2.Speed)
            {
                DataGridViewColumn col = new DataGridViewTextBoxColumn();

                if (SMode2.UsEuro == true)
                {
                    col.HeaderText = "Speed/ " + SMode2.SpeedMetric;
                }

                else
                {
                    col.HeaderText = "Speed/ " + SMode2.SpeedMetric;
                }


                col.Name = "Speed";
                int colIndex = dataGridView2.Columns.Add(col);

            }
            if (SMode2.Cadence)
            {
                DataGridViewColumn col = new DataGridViewTextBoxColumn();
                col.HeaderText = "Cadence";
                col.Name = "Cadence";
                int colIndex = dataGridView2.Columns.Add(col);

            }
            if (SMode2.Altitude)
            {
                DataGridViewColumn col = new DataGridViewTextBoxColumn();
                col.HeaderText = "Altitude";
                col.Name = "Altitude";
                int colIndex = dataGridView2.Columns.Add(col);

            }
            if (SMode2.Power)
            {

                textBox1.Enabled = true;
                if (FTP)
                {
                    DataGridViewColumn col = new DataGridViewTextBoxColumn();
                    col.HeaderText = "% of FTP";
                    col.Name = "Power";
                    int colIndex = dataGridView2.Columns.Add(col);
                }
                else
                {
                    DataGridViewColumn col = new DataGridViewTextBoxColumn();
                    col.HeaderText = "Power";
                    col.Name = "Power";
                    int colIndex = dataGridView2.Columns.Add(col);
                }

            }
            else
            {
                textBox1.Enabled = false;
            }
            if (SMode2.PPedalIndex)
            {
                DataGridViewColumn col1 = new DataGridViewTextBoxColumn();
                col1.HeaderText = "Power Balance";
                col1.Name = "PBalance";
                int colIndex1 = dataGridView2.Columns.Add(col1);

                DataGridViewColumn col = new DataGridViewTextBoxColumn();
                col.HeaderText = "Pedal Index";
                col.Name = "PPedalIndex";
                int colIndex = dataGridView2.Columns.Add(col);

            }
            if (SMode2.Set)
            {
                for (int i = 0; i < HRData2.HR.Length; i++)
                {

                    //Create the new row first and get the index of the new row
                    int rowIndex = this.dataGridView2.Rows.Add();

                    //Obtain a reference to the newly created DataGridViewRow 
                    var row = this.dataGridView2.Rows[rowIndex];



                    if (MaxHR)
                    {
                        row.Cells["HR"].Value = HRData2.HRPercent[i] + "%";
                    }
                    else
                    {
                        row.Cells["HR"].Value = HRData2.HR[i];
                    }



                    if (SMode2.Speed)
                    {

                        if (SMode2.UsEuro == true)
                        {
                            row.Cells["Speed"].Value = HRData2.Speed[i] + "" + SMode2.SpeedMetric;
                        }

                        else
                        {
                            row.Cells["Speed"].Value = HRData2.Speed[i] + "" + SMode2.SpeedMetric;
                        }
                    }
                    if (SMode2.Cadence)
                    {
                        row.Cells["Cadence"].Value = HRData2.Cadence[i];
                    }
                    if (SMode2.Altitude)
                    {
                        row.Cells["Altitude"].Value = HRData2.Altitude[i];
                    }
                    if (SMode2.Power)
                    {

                        if (FTP)
                        {
                            row.Cells["Power"].Value = HRData2.PowerPercent[i] + "%";
                        }
                        else
                        {
                            row.Cells["Power"].Value = HRData2.Power[i] + "W";
                        }
                    }
                    if (SMode2.PPedalIndex)
                    {
                        row.Cells["PBalance"].Value = HRData2.powerbalance[i];
                        row.Cells["PPedalIndex"].Value = HRData2.pedalindex[i];
                    }



                }
            }
            if (SMode2.Set)
            {
                bool isfirst = false;
                calc_norm_power(isfirst);
                label38.Text = NP2.ToString() + " watts";
                label38.Visible = true;
            }




        }

        /// <summary>
        /// Updates the graph with the second set of curves for file comparison.
        /// </summary>
        private void UpdateChart2()
        {

            GraphPane myPane = zedGraphControl1.GraphPane;
            zedGraphControl1.IsShowPointValues = true;
            myPane.Legend.IsVisible = false;
            myPane.Title.IsVisible = false;
            myPane.XAxis.MajorGrid.IsVisible = false;
            myPane.XAxis.MinorGrid.IsVisible = false;
            myPane.YAxis.MajorGrid.IsVisible = false;
            myPane.YAxis.MinorGrid.IsVisible = false;
            myPane.YAxis.IsVisible = false;
            myPane.XAxis.IsVisible = false;

            myPane.XAxis.Title.Text = "Time (Seconds)";
            myPane.XAxis.Type = AxisType.Linear;
            myPane.XAxis.Scale.MajorStep = 10;
            myPane.XAxis.Scale.MinorStep = 15;
            myPane.XAxis.Scale.Min = 0; // start time

            for (int i = 0; i < HRData2.HR.Length; i++)
            {
                if (MaxHR)
                {
                    hr2.Add(HRData2.Intervals[i], HRData2.HRPercent[i]);
                }
                else
                {
                    hr2.Add(HRData2.Intervals[i], HRData2.HR[i]);
                }



                if (SMode2.Speed)
                {
                    Speed2.Add(HRData2.Intervals[i], HRData2.Speed[i]);
                }
                if (SMode2.Cadence)
                {
                    Cadence2.Add(HRData2.Intervals[i], HRData2.Cadence[i]);
                }
                if (SMode2.Altitude)
                {
                    Altitude2.Add(HRData2.Intervals[i], HRData2.Altitude[i]);
                }
                if (SMode2.Power)
                {
                    if (FTP)
                    {
                        Power2.Add(HRData2.Intervals[i], HRData2.PowerPercent[i]);
                    }
                    else
                    {
                        Power2.Add(HRData2.Intervals[i], HRData2.Power[i]);
                    }

                }
                if (SMode2.PPedalIndex)
                {
                    PPedalIndex2.Add(HRData2.Intervals[i], HRData2.PPedalIndex[i]);
                }
            }



            LineItem myCurve = new LineItem("Heart Rate two",
                          hr2, Color.LightSalmon, SymbolType.None)
            { YAxisIndex = myPane.YAxisList.IndexOf(y0) };
            myCurve.Line.Width = 1.0F;
            myCurve.Line.Color = Color.FromArgb(70, Color.LightSalmon);

            myPane.CurveList.Add(myCurve);

            if (SMode2.Speed)
            {

                LineItem myCurve2 = new LineItem("Speed two",
                         Speed2, Color.LightSkyBlue, SymbolType.None)
                { YAxisIndex = myPane.YAxisList.IndexOf(y1) };
                myCurve2.Line.Width = 1.0F;
                myCurve2.Line.Color = Color.FromArgb(70, Color.LightSkyBlue);
                myPane.CurveList.Add(myCurve2);


            }
            if (SMode2.Cadence)
            {

                LineItem myCurve3 = new LineItem("Cadence two",
                         Cadence2, Color.DarkGray, SymbolType.None)
                { YAxisIndex = myPane.YAxisList.IndexOf(y2) };
                myCurve3.Line.Width = 1.0F;
                myCurve3.Line.Color = Color.FromArgb(70, Color.DarkGray);
                myPane.CurveList.Add(myCurve3);

            }
            if (SMode2.Power)
            {


                LineItem myCurve5 = new LineItem("Power two",
                          Power2, Color.Orange, SymbolType.None)
                { YAxisIndex = myPane.YAxisList.IndexOf(y3) };
                myCurve5.Line.Width = 1.0F;
                myCurve5.Line.Color = Color.FromArgb(70, Color.Orange);
                myPane.CurveList.Add(myCurve5);
            }
            if (SMode2.Altitude)
            {

                LineItem myCurve4 = new LineItem("Altitude two",
                          Altitude2, Color.Gray, SymbolType.None)
                { YAxisIndex = myPane.YAxisList.IndexOf(y4) };
                myCurve4.Line.Width = 1.0F;
                myCurve4.Line.Color = Color.FromArgb(70, Color.Gray);
                myPane.CurveList.Add(myCurve4);

            }
            if (SMode2.PPedalIndex)
            {


            }

            zedGraphControl1.AxisChange();
            zedGraphControl1.Invalidate();
            this.Refresh();


        }

        /// <summary>
        /// Adds the first curves onto the graph and sets main graph settings.
        /// </summary>
        private void UpdateChart()
        {

            if (SMode.UsEuro == true)
            {
                comboBox1.SelectedIndex = 0;
            }
            else
            {
                comboBox1.SelectedIndex = 1;
            }

            GraphPane myPane = zedGraphControl1.GraphPane;
            zedGraphControl1.IsShowPointValues = true;
            myPane.Legend.IsVisible = false;
            myPane.Title.IsVisible = false;
            myPane.XAxis.MajorGrid.IsVisible = false;
            myPane.XAxis.MinorGrid.IsVisible = false;
            myPane.YAxis.MajorGrid.IsVisible = false;
            myPane.YAxis.MinorGrid.IsVisible = false;
            myPane.YAxis.IsVisible = false;
            myPane.XAxis.IsVisible = false;
            myPane.XAxis.Title.Text = "Time (Seconds)";
            myPane.XAxis.Type = AxisType.Linear;
            myPane.XAxis.Scale.MajorStep = 10;
            myPane.XAxis.Scale.MinorStep = 15;
            myPane.XAxis.Scale.Min = 0; // start time

            for (int i = 0; i < HRData.HR.Length; i++)
            {

                if (MaxHR)
                {
                    hr.Add(HRData.Intervals[i], HRData.HRPercent[i]);
                }
                else
                {
                    hr.Add(HRData.Intervals[i], HRData.HR[i]);
                }



                if (SMode.Speed)
                {
                    Speed.Add(HRData.Intervals[i], HRData.Speed[i]);
                }
                if (SMode.Cadence)
                {
                    Cadence.Add(HRData.Intervals[i], HRData.Cadence[i]);
                }
                if (SMode.Altitude)
                {
                    Altitude.Add(HRData.Intervals[i], HRData.Altitude[i]);
                }
                if (SMode.Power)
                {
                    if (FTP)
                    {
                        Power.Add(HRData.Intervals[i], HRData.PowerPercent[i]);
                    }
                    else
                    {
                        Power.Add(HRData.Intervals[i], HRData.Power[i]);
                    }

                }
                if (SMode.PPedalIndex)
                {
                    PPedalIndex.Add(HRData.Intervals[i], HRData.PPedalIndex[i]);
                }
            }

            y0 = new YAxis("Heart Rate");
            y0.Scale.FontSpec.FontColor = Color.LightSalmon;
            y0.Title.FontSpec.FontColor = Color.LightSalmon;

            myPane.YAxisList.Add(y0);
            LineItem HrCurve = new LineItem("Heart Rate",
                        hr, Color.LightSalmon, SymbolType.None)
            { YAxisIndex = myPane.YAxisList.IndexOf(y0) };

            myPane.CurveList.Add(HrCurve);

            if (SMode.Speed)
            {

                y1 = new YAxis("Speed");
                y1.Scale.FontSpec.FontColor = Color.LightSkyBlue;
                y1.Title.FontSpec.FontColor = Color.LightSkyBlue;
                myPane.YAxisList.Add(y1);
                LineItem myCurve2 = new LineItem("Speed",
                         Speed, Color.LightSkyBlue, SymbolType.None)
                { YAxisIndex = myPane.YAxisList.IndexOf(y1) };
                myCurve2.Line.Width = 1.0F;

                myPane.CurveList.Add(myCurve2);
            }
            if (SMode.Cadence)
            {
                y2 = new YAxis("Cadence");
                y2.Scale.FontSpec.FontColor = Color.DarkGray;
                y2.Title.FontSpec.FontColor = Color.DarkGray;
                myPane.YAxisList.Add(y2);
                LineItem myCurve3 = new LineItem("Cadence",
                         Cadence, Color.DarkGray, SymbolType.None)
                { YAxisIndex = myPane.YAxisList.IndexOf(y2) };
                myCurve3.Line.Width = 1.0F;
                myPane.CurveList.Add(myCurve3);

            }
            if (SMode.Power)
            {
                y3 = new YAxis("Cadence");
                y3.Scale.FontSpec.FontColor = Color.Orange;
                y3.Title.FontSpec.FontColor = Color.Orange;
                myPane.YAxisList.Add(y3);
                LineItem myCurve5 = new LineItem("Power",
                          Power, Color.Orange, SymbolType.None)
                { YAxisIndex = myPane.YAxisList.IndexOf(y3) };
                myCurve5.Line.Width = 1.0F;
                myPane.CurveList.Add(myCurve5);

            }
            if (SMode.Altitude)
            {
                y4 = new YAxis("Cadence");
                y4.Scale.FontSpec.FontColor = Color.Gray;
                y4.Title.FontSpec.FontColor = Color.Gray;
                myPane.YAxisList.Add(y4);
                LineItem myCurve4 = new LineItem("Altitude",
                          Altitude, Color.Gray, SymbolType.None)
                { YAxisIndex = myPane.YAxisList.IndexOf(y4) };
                myCurve4.Line.Width = 1.0F;
                myPane.CurveList.Add(myCurve4);
            }
            if (SMode.PPedalIndex)
            {


            }

            foreach (var axis in myPane.YAxisList)
            {
                axis.Scale.FontSpec.Size = 6.0f;
                axis.Scale.FontSpec.IsBold = false;
                axis.Title.FontSpec.Size = 6.0f;


            }




            zedGraphControl1.AxisChange();
            zedGraphControl1.Invalidate();
            this.Refresh();


        }

        /// <summary>
        /// Updates the main display components. such as the main datagridviews and the summary data.
        /// </summary>
        private void UpdateDisplay()
        {
            SetAverageData();


            label12.Visible = true;
            label12.Text = HRData.StartDate.ToString("dd/mm/yyyy") + "/" + HRData.StartTime.ToString("hh:mm:ss");
            label12.Text += " - D: " + Params.Length + " - I: " + Params.Interval + "s";

            if (SMode.UsEuro == false)
            {
                label16.Visible = true;
                label16.Text = (TotalDistance) + "km";
                label17.Visible = true;
                label17.Text = (MaxSpeed) + "km";
                label21.Visible = true;
                label21.Text = (AverageSpeed) + "km";
            }
            else
            {
                label16.Text = (TotalDistance) + "mi";
                label16.Visible = true;
                label17.Text = (MaxSpeed) + "mi";
                label17.Visible = true;
                label21.Text = (AverageSpeed) + "mi";
                label21.Visible = true;
            }

            label18.Visible = true;
            label19.Visible = true;
            label20.Visible = true;
            label18.Text = (AMaxHR) + "bpm";
            label19.Text = (MaxPower) + "W";
            label20.Text = (MaxAltitude) + "m";


            label22.Visible = true;
            label23.Visible = true;
            label24.Visible = true;
            label25.Visible = true;
            label22.Text = (AverageHR) + "bpm";
            label23.Text = (MinHR) + "bpm";
            label24.Text = (AveragePower) + "bpm";
            label25.Text = (AverageAlt) + "m";



            if (MaxHR)
            {
                DataGridViewColumn col = new DataGridViewTextBoxColumn();
                col.HeaderText = "% of Max HR";
                col.Name = "HR";
                int colIndex1 = dataGridView1.Columns.Add(col);
            }
            else
            {
                DataGridViewColumn col = new DataGridViewTextBoxColumn();
                col.HeaderText = "Heart Rate (BPM)";
                col.Name = "HR";
                int colIndex = dataGridView1.Columns.Add(col);
            }


            if (SMode.Speed)
            {
                DataGridViewColumn col = new DataGridViewTextBoxColumn();

                if (SMode.UsEuro == true)
                {
                    col.HeaderText = "Speed/ " + SMode.SpeedMetric;
                }

                else
                {
                    col.HeaderText = "Speed/ " + SMode.SpeedMetric;
                }


                col.Name = "Speed";
                int colIndex = dataGridView1.Columns.Add(col);

            }
            if (SMode.Cadence)
            {
                DataGridViewColumn col = new DataGridViewTextBoxColumn();
                col.HeaderText = "Cadence";
                col.Name = "Cadence";
                int colIndex = dataGridView1.Columns.Add(col);

            }
            if (SMode.Altitude)
            {
                DataGridViewColumn col = new DataGridViewTextBoxColumn();
                col.HeaderText = "Altitude";
                col.Name = "Altitude";
                int colIndex = dataGridView1.Columns.Add(col);

            }
            if (SMode.Power)
            {


                textBox1.Enabled = true;
                if (FTP)
                {
                    DataGridViewColumn col = new DataGridViewTextBoxColumn();
                    col.HeaderText = "% of FTP";
                    col.Name = "Power";
                    int colIndex = dataGridView1.Columns.Add(col);
                }
                else
                {
                    DataGridViewColumn col = new DataGridViewTextBoxColumn();
                    col.HeaderText = "Power";
                    col.Name = "Power";
                    int colIndex = dataGridView1.Columns.Add(col);
                }

            }
            else
            {
                textBox1.Enabled = false;
            }
            if (SMode.PPedalIndex)
            {
                DataGridViewColumn col1 = new DataGridViewTextBoxColumn();
                col1.HeaderText = "Power Balance";
                col1.Name = "PBalance";
                int colIndex1 = dataGridView1.Columns.Add(col1);

                DataGridViewColumn col = new DataGridViewTextBoxColumn();
                col.HeaderText = "Pedal Index";
                col.Name = "PPedalIndex";
                int colIndex = dataGridView1.Columns.Add(col);

            }


            for (int i = 0; i < HRlist.Length; i++)
            {

                //Create the new row first and get the index of the new row
                int rowIndex = this.dataGridView1.Rows.Add();

                //Obtain a reference to the newly created DataGridViewRow 
                var row = this.dataGridView1.Rows[rowIndex];



                if (MaxHR)
                {
                    row.Cells["HR"].Value = HRPercentlist[i] + "%";
                }
                else
                {
                    row.Cells["HR"].Value = HRlist[i];
                }



                if (SMode.Speed)
                {

                    if (SMode.UsEuro == true)
                    {
                        row.Cells["Speed"].Value = Speedlist[i] + "" + SMode.SpeedMetric;
                    }

                    else
                    {
                        row.Cells["Speed"].Value = Speedlist[i] + "" + SMode.SpeedMetric;
                    }
                }
                if (SMode.Cadence)
                {
                    row.Cells["Cadence"].Value = Cadencelist[i];
                }
                if (SMode.Altitude)
                {
                    row.Cells["Altitude"].Value = Altitudelist[i];
                }
                if (SMode.Power)
                {

                    if (FTP)
                    {
                        row.Cells["Power"].Value = PowerPercentlist[i] + "%";
                    }
                    else
                    {
                        row.Cells["Power"].Value = Powerlist[i] + "W";
                    }
                }
                if (SMode.PPedalIndex)
                {
                    row.Cells["PBalance"].Value = HRData.powerbalance[i];
                    row.Cells["PPedalIndex"].Value = HRData.pedalindex[i];

                }



            }

            calc_norm_power();
            label37.Text = NP1.ToString()+" watts";
            label37.Visible = true;



        }

        /// <summary>
        /// When the new file menu item is clicked This even will launch the file explorer wizard and then check the file selected by the user exists,
        /// then forwarding it's absolute path to the dataload method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {

            clearDisplay();
            clearChart1();
            Stream stream = null;
            OpenFileDialog newFile = new OpenFileDialog();
            DialogResult result = newFile.ShowDialog();
            SecondFile = false;
            if (result == DialogResult.OK && (stream = newFile.OpenFile()) != null)
            {


                pathToFile = newFile.FileName;

                if (File.Exists(pathToFile))
                {
                    dataLoad(pathToFile, SecondFile);

                }
                else
                {
                    MessageBox.Show("Error");
                }

            }
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }
        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }
        private void enterFTPMaxHRToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        private void label14_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// If the user changes the combobox values this method will either set the values to percentage of user inputs of absolute values from the hrm file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedValue.ToString() == "2")
            {
                FTP = false;
                MaxHR = false;
                clearDisplay();
                clearChart1();
                UpdateDisplay();
                UpdateChart();
            }
            if (comboBox2.SelectedValue.ToString() == "1")
            {
                if (FTP)
                {
                    HRData.ConvertPower(textBox1.Text);
                    
                    PowerPercentlist = HRData.PowerPercent;
                    

                    if (SMode2.Set)
                    {
                        HRData2.ConvertPower(textBox1.Text);
                        PowerPercentlist1 = HRData2.PowerPercent;
                    }

                }
                if (MaxHR)
                {
                    HRData.ConvertRate(textBox2.Text);
                    HRPercentlist = HRData.HRPercent;

                    if (SMode2.Set)
                    {
                        HRData2.ConvertRate(textBox2.Text);
                        HRPercentlist1 = HRData2.HRPercent;
                    }
                }




                clearDisplay();
                clearChart1();
                UpdateDisplay();
                UpdateChart();
            }
        }
        /// <summary>
        /// This method is an event handler for the graph zoom. Upon zooming the data lists used for displaying will be updated and the update display method recalled.
        /// </summary>
        /// <param name="sender">contains the graph pane data.</param>
        /// <param name="oldState"></param>
        /// <param name="newState"></param>
        private void zedGraphControl1_ZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState)
        {

            int startpoint = (int)sender.GraphPane.XAxis.Scale.Min;
            int endpoint = (int)sender.GraphPane.XAxis.Scale.Max;
            int bpoint = (int)sender.GraphPane.YAxis.Scale.Min;
            int tpoint = (int)sender.GraphPane.YAxis.Scale.Max;

            int difference = endpoint - startpoint;


            if (HRData.HR.Length > startpoint && HRData.HR.Length >= endpoint)
            {
                //bool zoomin = true;
                List<double> HRLIST = HRData.HR.ToList();
                List<double> updateheartrate;
                if (startpoint < 0)
                {
                    startpoint = System.Math.Abs(startpoint);
                }
                updateheartrate = HRLIST.GetRange(startpoint, difference);
                HRlist = updateheartrate.ToArray();


            }

            if (HRData.Speed.Length > startpoint && HRData.Speed.Length >= endpoint)
            {

                if (SMode.Speed)
                {
                    List<double> speedlist = HRData.Speed.ToList();
                    List<double> speedupdate;
                    speedupdate = speedlist.GetRange(startpoint, difference);
                    Speedlist = speedupdate.ToArray();
                }
            }

            if (HRData.Altitude.Length > startpoint && HRData.Altitude.Length >= endpoint)
            {

                if (SMode.Altitude)
                {
                    List<double> altlist = HRData.Altitude.ToList();
                    List<double> altupdate;
                    altupdate = altlist.GetRange(startpoint, difference);
                    Altitudelist = altupdate.ToArray();
                }

            }

            if (HRData.Cadence.Length > startpoint && HRData.Cadence.Length >= endpoint)
            {


                if (SMode.Cadence)
                {
                    List<double> cadencelist = HRData.Cadence.ToList();
                    List<double> cadenceupdate;
                    cadenceupdate = cadencelist.GetRange(startpoint, difference);
                    Cadencelist = cadenceupdate.ToArray();
                }


            }

            if (HRData.Power.Length > startpoint && HRData.Power.Length >= endpoint)
            {

                if (SMode.Power)
                {
                    List<double> powerlist = HRData.Power.ToList();
                    List<double> powerupdate;
                    powerupdate = powerlist.GetRange(startpoint, difference);
                    Powerlist = powerupdate.ToArray();

                }

            }
            SetAverageData();

            if (SMode2.Set)
            {
                if (HRData2.HR.Length > startpoint && HRData2.HR.Length >= endpoint)
                {
                    //bool zoomin = true;
                    List<double> HRLIST = HRData2.HR.ToList();
                    List<double> updateheartrate;
                    if (startpoint < 0)
                    {
                        startpoint = System.Math.Abs(startpoint);
                    }
                    updateheartrate = HRLIST.GetRange(startpoint, difference);
                    HRlist1 = updateheartrate.ToArray();
                }

                if (HRData2.Speed.Length > startpoint && HRData2.Speed.Length >= endpoint)
                {

                    if (SMode2.Speed)
                    {
                        List<double> speedlist = HRData2.Speed.ToList();
                        List<double> speedupdate;
                        speedupdate = speedlist.GetRange(startpoint, difference);
                        Speedlist1 = speedupdate.ToArray();
                    }
                }
                if (HRData2.Altitude.Length > startpoint && HRData2.Speed.Length >= endpoint)
                {
                    if (SMode2.Altitude)
                    {
                        List<double> altlist = HRData2.Altitude.ToList();
                        List<double> altupdate;
                        altupdate = altlist.GetRange(startpoint, difference);
                        Altitudelist1 = altupdate.ToArray();
                    }

                }
                if (HRData2.Cadence.Length > startpoint && HRData2.Speed.Length >= endpoint)
                {

                    if (SMode2.Cadence)
                    {
                        List<double> cadencelist = HRData2.Cadence.ToList();
                        List<double> cadenceupdate;
                        cadenceupdate = cadencelist.GetRange(startpoint, difference);
                        Cadencelist1 = cadenceupdate.ToArray();
                    }
                }
                if (HRData2.Power.Length > startpoint && HRData2.Speed.Length >= endpoint)
                {

                    if (SMode2.Power)
                    {
                        List<double> powerlist = HRData2.Power.ToList();
                        List<double> powerupdate;
                        powerupdate = powerlist.GetRange(startpoint, difference);
                        Powerlist1 = powerupdate.ToArray();
                    }

                }
                SetAverageData();
            }

            Console.WriteLine("zooooom");

            clearDisplay();
            UpdateDisplay();
            UpdateDisplay2();
        }

        /// <summary>
        /// Sets the average data for display.
        /// </summary>
        private void SetAverageData()
        {

            TotalDistance = Math.Round((Math.Round(HRData.Average(Speedlist), 2)) * HRData.TimeToDouble(Params.Length), 2);


            AMaxHR = HRlist.Max();
            MinHR = HRlist.Min();
            AverageHR = Math.Round(HRData.Average(HRlist), 2);

            if (SMode.Power == true)
            {
                AveragePower = Math.Round(HRData.Average(Powerlist), 2);
                MaxPower = Powerlist.Max();
            }
            else
            {
                AveragePower = 0;
                MaxPower = 0;
            }

            if (SMode.Speed == true)
            {
                if (SMode.UsEuro == true) // US
                {
                    MaxSpeed = Speedlist.Max();
                    AverageSpeed = Math.Round(HRData.Average(Speedlist), 2);
                }
                else // false = //Euro
                {
                    MaxSpeed = Speedlist.Max();
                    AverageSpeed = Math.Round(HRData.Average(Speedlist), 2);
                }
            }
            else
            {
                AverageSpeed = 0;
                MaxSpeed = 0;
            }

            if (SMode.Altitude == true)
            {
                if (SMode.UsEuro == true) // US
                {
                    MaxAltitude = Altitudelist.Max();
                    AverageAlt = Math.Round(HRData.Average(Altitudelist), 2);
                }
                else // false = //Euro
                {
                    MaxAltitude = Altitudelist.Max();
                    AverageAlt = Math.Round(HRData.Average(Altitudelist), 2);
                }
            }
            else
            {
                AverageAlt = 0;
                MaxAltitude = 0;
            }

            if (SMode2.Set)
            {
                TotalDistance1 = Math.Round((Math.Round(HRData.Average(Speedlist1), 2)) * HRData.TimeToDouble(Params2.Length), 2);


                AMaxHR1 = HRlist1.Max();
                MinHR1 = HRlist1.Min();
                AverageHR1 = Math.Round(HRData.Average(HRlist1), 2);

                if (SMode2.Power == true)
                {
                    AveragePower1 = Math.Round(HRData.Average(Powerlist1), 2);
                    MaxPower1 = Powerlist1.Max();
                }
                else
                {
                    AveragePower1 = 0;
                    MaxPower1 = 0;
                }

                if (SMode2.Speed == true)
                {
                    if (SMode2.UsEuro == true) // US
                    {
                        MaxSpeed1 = Speedlist1.Max();
                        AverageSpeed1 = Math.Round(HRData.Average(Speedlist1), 2);
                    }
                    else // false = //Euro
                    {
                        MaxSpeed1 = Speedlist1.Max();
                        AverageSpeed1 = Math.Round(HRData.Average(Speedlist1), 2);
                    }
                }
                else
                {
                    AverageSpeed1 = 0;
                    MaxSpeed1 = 0;
                }

                if (SMode2.Altitude == true)
                {
                    if (SMode2.UsEuro == true) // US
                    {
                        MaxAltitude1 = Altitudelist1.Max();
                        AverageAlt1 = Math.Round(HRData.Average(Altitudelist1), 2);
                    }
                    else // false = //Euro
                    {
                        MaxAltitude1 = Altitudelist1.Max();
                        AverageAlt1 = Math.Round(HRData.Average(Altitudelist1), 2);
                    }
                }
                else
                {
                    AverageAlt1 = 0;
                    MaxAltitude1 = 0;
                }
            }





        }

        private static PointPairList VisiblePoints(ZedGraphControl control, LineItem lineItem, PointPairList points)
        {
            var pointPairList = new PointPairList();
            pointPairList.AddRange(points.Where(pp => IsVisible(control, lineItem, pp)).ToList());
            return pointPairList;
        }

        private static bool IsVisible(ZedGraphControl control, LineItem lineItem, PointPair point)
        {
            GraphPane pane = control.GraphPane;
            Scale xScale = lineItem.GetXAxis(pane).Scale;
            Scale yScale = lineItem.GetYAxis(pane).Scale;
            return point.X > xScale.Min && point.X < xScale.Max && point.Y > yScale.Min && point.Y < yScale.Max;
        }

        /// <summary>
        /// Will run the dataload method on the comparison file path in the same way the new file event does.
        /// </summary>
        private void newComparisonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stream stream = null;
            OpenFileDialog newFile = new OpenFileDialog();
            DialogResult result = newFile.ShowDialog();
            SecondFile = true;
            if (result == DialogResult.OK && (stream = newFile.OpenFile()) != null)
            {


                pathToFile = newFile.FileName;

                if (File.Exists(pathToFile))
                {
                    dataLoad(pathToFile, SecondFile);

                }
                else
                {
                    MessageBox.Show("Error");
                }

            }
        }

        /// <summary>
        /// calulates the difference between the comparison data and updates the tooltip depending on where the users mouse pointer is.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="pane"></param>
        /// <param name="curve">the particular curve the users mouse is pointed to.</param>
        /// <param name="iPt">The point which the mouse falls on triggering the event.</param>
        /// <returns></returns>
        private string zedGraphControl1_PointValueEvent(ZedGraphControl sender, GraphPane pane, CurveItem curve, int iPt)
        {
            //IPointList plist = pane.CurveList[0].Points;
            string tooltip = null;


            if (curve.Label.Text == "Heart Rate")
            {
                if (curve.NPts > iPt)
                {

                    if (SMode2.Set)
                    {
                        foreach (CurveItem cve in pane.CurveList)
                        {
                            if (cve.Label.Text == "Heart Rate two")
                            {
                                if (cve.NPts > iPt)
                                {
                                    double remainder = curve[iPt].Y - cve[iPt].Y;
                                    string difference = null;
                                    if (remainder < 0)
                                    {
                                        difference = remainder.ToString();
                                    }
                                    else
                                    {
                                        difference = "+" + remainder.ToString();
                                    }

                                    tooltip = String.Format(curve.Label.Text + "/" + cve.Label.Text + curve[iPt].Y + "bpm /" + cve[iPt].Y + "bpm " + difference);
                                    break;
                                }
                                else
                                {
                                    tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y + "bpm");
                                }

                            }

                        }
                    }
                    else
                    {
                        tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y + "bpm");
                    }



                }

            }
            if (curve.Label.Text == "Speed")
            {
                if (curve.NPts > iPt)
                {

                    if (SMode2.Set)
                    {
                        foreach (CurveItem cve in pane.CurveList)
                        {
                            if (cve.Label.Text == "Speed two")
                            {
                                if (cve.NPts > iPt)
                                {
                                    double remainder = curve[iPt].Y - cve[iPt].Y;
                                    string difference = null;
                                    if (remainder < 0)
                                    {
                                        difference = remainder.ToString();
                                    }
                                    else
                                    {
                                        difference = "+" + remainder.ToString();
                                    }



                                    tooltip = String.Format(curve.Label.Text + "/" + cve.Label.Text + curve[iPt].Y + "/" + cve[iPt].Y + SMode2.SpeedMetric + " " + difference + SMode2.SpeedMetric);
                                    break;
                                }
                                else
                                {
                                    tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y + SMode2.SpeedMetric);
                                }
                            }

                        }
                    }
                    else
                    {
                        tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y + SMode2.SpeedMetric);
                    }



                }
            }
            if (curve.Label.Text == "Cadence")
            {
                if (curve.NPts > iPt)
                {

                    if (SMode2.Set)
                    {
                        foreach (CurveItem cve in pane.CurveList)
                        {
                            if (cve.Label.Text == "Cadence two")
                            {
                                if (cve.NPts > iPt)
                                {
                                    double remainder = curve[iPt].Y - cve[iPt].Y;
                                    string difference = null;
                                    if (remainder < 0)
                                    {
                                        difference = remainder.ToString();
                                    }
                                    else
                                    {
                                        difference = "+" + remainder.ToString();
                                    }

                                    tooltip = String.Format(curve.Label.Text + "/" + cve.Label.Text + curve[iPt].Y + "/" + cve[iPt].Y + " " + difference);
                                }
                                else
                                {
                                    tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y);
                                }
                            }

                        }
                    }
                    else
                    {
                        tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y);
                    }



                }
            }
            if (curve.Label.Text == "Altitude")
            {
                if (curve.NPts > iPt)
                {

                    if (SMode2.Set)
                    {
                        foreach (CurveItem cve in pane.CurveList)
                        {
                            if (cve.Label.Text == "Altitude two")
                            {
                                if (cve.NPts > iPt)
                                {
                                    double remainder = curve[iPt].Y - cve[iPt].Y;
                                    string difference = null;
                                    if (remainder < 0)
                                    {
                                        difference = remainder.ToString();
                                    }
                                    else
                                    {
                                        difference = "+" + remainder.ToString();
                                    }

                                    tooltip = String.Format(curve.Label.Text + "/" + cve.Label.Text + curve[iPt].Y + "/" + cve[iPt].Y + "m " + difference + "m");
                                }
                                else
                                {
                                    tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y);
                                }
                            }

                        }
                    }
                    else
                    {
                        tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y);
                    }



                }
            }
            if (curve.Label.Text == "Power")
            {
                if (curve.NPts > iPt)
                {

                    if (SMode2.Set)
                    {
                        foreach (CurveItem cve in pane.CurveList)
                        {
                            if (cve.Label.Text == "Power two")
                            {
                                if (cve.NPts > iPt)
                                {
                                    double remainder = curve[iPt].Y - cve[iPt].Y;
                                    string difference = null;
                                    if (remainder < 0)
                                    {
                                        difference = remainder.ToString();
                                    }
                                    else
                                    {
                                        difference = "+" + remainder.ToString();
                                    }

                                    tooltip = String.Format(curve.Label.Text + "/" + cve.Label.Text + curve[iPt].Y + "/" + cve[iPt].Y + "W " + difference + "W");
                                }
                                else
                                {
                                    tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y);
                                }
                            }

                        }
                    }
                    else
                    {
                        tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y);
                    }



                }
            }

            if (curve.Label.Text == "Heart Rate two")
            {
                if (curve.NPts > iPt)
                {

                    if (SMode.Set)
                    {
                        foreach (CurveItem cve in pane.CurveList)
                        {
                            if (cve.Label.Text == "Heart Rate")
                            {
                                if (cve.NPts > iPt)
                                {
                                    double remainder = curve[iPt].Y - cve[iPt].Y;
                                    string difference = null;
                                    if (remainder < 0)
                                    {
                                        difference = remainder.ToString();
                                    }
                                    else
                                    {
                                        difference = "+" + remainder.ToString();
                                    }

                                    tooltip = String.Format(curve.Label.Text + "/" + cve.Label.Text + curve[iPt].Y + "bpm /" + cve[iPt].Y + "bpm " + difference);
                                    break;
                                }
                                else
                                {
                                    tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y + "bpm");
                                }

                            }

                        }
                    }
                    else
                    {
                        tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y + "bpm");
                    }



                }

            }
            if (curve.Label.Text == "Speed two")
            {
                if (curve.NPts > iPt)
                {

                    if (SMode.Set)
                    {
                        foreach (CurveItem cve in pane.CurveList)
                        {
                            if (cve.Label.Text == "Speed")
                            {
                                if (cve.NPts > iPt)
                                {
                                    double remainder = curve[iPt].Y - cve[iPt].Y;
                                    string difference = null;
                                    if (remainder < 0)
                                    {
                                        difference = remainder.ToString();
                                    }
                                    else
                                    {
                                        difference = "+" + remainder.ToString();
                                    }



                                    tooltip = String.Format(curve.Label.Text + "/" + cve.Label.Text + curve[iPt].Y + "/" + cve[iPt].Y + SMode2.SpeedMetric + " " + difference + SMode2.SpeedMetric);
                                    break;
                                }
                                else
                                {
                                    tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y + SMode2.SpeedMetric);
                                }
                            }

                        }
                    }
                    else
                    {
                        tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y + SMode2.SpeedMetric);
                    }



                }
            }
            if (curve.Label.Text == "Cadence two")
            {
                if (curve.NPts > iPt)
                {

                    if (SMode.Set)
                    {
                        foreach (CurveItem cve in pane.CurveList)
                        {
                            if (cve.Label.Text == "Cadence")
                            {
                                if (cve.NPts > iPt)
                                {
                                    double remainder = curve[iPt].Y - cve[iPt].Y;
                                    string difference = null;
                                    if (remainder < 0)
                                    {
                                        difference = remainder.ToString();
                                    }
                                    else
                                    {
                                        difference = "+" + remainder.ToString();
                                    }

                                    tooltip = String.Format(curve.Label.Text + "/" + cve.Label.Text + curve[iPt].Y + "/" + cve[iPt].Y + " " + difference);
                                }
                                else
                                {
                                    tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y);
                                }
                            }

                        }
                    }
                    else
                    {
                        tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y);
                    }



                }
            }
            if (curve.Label.Text == "Altitude two")
            {
                if (curve.NPts > iPt)
                {

                    if (SMode.Set)
                    {
                        foreach (CurveItem cve in pane.CurveList)
                        {
                            if (cve.Label.Text == "Altitude")
                            {
                                if (cve.NPts > iPt)
                                {
                                    double remainder = curve[iPt].Y - cve[iPt].Y;
                                    string difference = null;
                                    if (remainder < 0)
                                    {
                                        difference = remainder.ToString();
                                    }
                                    else
                                    {
                                        difference = "+" + remainder.ToString();
                                    }

                                    tooltip = String.Format(curve.Label.Text + "/" + cve.Label.Text + curve[iPt].Y + "/" + cve[iPt].Y + "m " + difference + "m");
                                }
                                else
                                {
                                    tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y);
                                }
                            }

                        }
                    }
                    else
                    {
                        tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y);
                    }



                }
            }
            if (curve.Label.Text == "Power two")
            {
                if (curve.NPts > iPt)
                {

                    if (SMode.Set)
                    {
                        foreach (CurveItem cve in pane.CurveList)
                        {
                            if (cve.Label.Text == "Power ")
                            {
                                if (cve.NPts > iPt)
                                {
                                    double remainder = curve[iPt].Y - cve[iPt].Y;
                                    string difference = null;
                                    if (remainder < 0)
                                    {
                                        difference = Math.Round(remainder, 2, MidpointRounding.AwayFromZero).ToString();
                                    }
                                    else
                                    {
                                        difference = "+" + Math.Round(remainder, 2, MidpointRounding.AwayFromZero).ToString();
                                    }

                                    tooltip = String.Format(curve.Label.Text + "/" + cve.Label.Text + curve[iPt].Y + "/" + cve[iPt].Y + "W " + difference + "W");
                                }
                                else
                                {
                                    tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y);
                                }
                            }

                        }
                    }
                    else
                    {
                        tooltip = String.Format(curve.Label.Text + ": " + curve[iPt].Y);
                    }



                }
            }



            return tooltip;
        }

        private static void LogVisibility(ZedGraphControl control, LineItem lineItem, PointPairList points)
        {
            List<PointPair> visiblePoints = VisiblePoints(control, lineItem, points);
            //Console.Out.WriteLine(DateTime.Now + ": " + string.Join(",", visiblePoints.Select(pp => string.Format("({0:N1}, {1:N1})", pp.X, pp.Y))));
        }
        /// <summary>
        /// This method will determine the FTP and FHR input by the user and determine the normalised power, IF and TSS
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBox1.Text) && String.IsNullOrEmpty(textBox2.Text))
            {
                MessageBox.Show("You must change atleast one of the fields.");
            }
            else
            {

                if (textBox1.Text.Trim() != string.Empty)
                {


                    if (SMode.Power)
                    {

                        
                        HRData.ConvertPower(textBox1.Text);
                        PowerPercentlist = HRData.PowerPercent;
                        double FTPint = Convert.ToInt32(textBox1.Text);
                        IF = NP1 / FTPint;
                        label43.Text = IF.ToString("0.00");
                        double TSS;
                        TSS = Math.Round(((HRData.Intervals.Length+1) * NP1 * IF) / (FTPint * 3600) * 100, 1);
                        label40.Text = TSS.ToString();
                        label40.Visible = true;
                        label43.Visible = true;

                        if (SMode2.Set)
                        {
                            HRData2.ConvertPower(textBox1.Text);
                            PowerPercentlist1 = HRData2.PowerPercent;
                            double FTPint1 = Convert.ToInt32(textBox1.Text);
                            IF2 = NP2 / FTPint1;
                            label44.Text = IF2.ToString("0.00");
                            double TSS1;
                            TSS1 = Math.Round(((HRData2.Intervals.Length + 1) * NP1 * IF) / (FTPint1 * 3600) * 100, 1);
                            label41.Text = TSS1.ToString();
                            label41.Visible = true;
                            label44.Visible = true;
                        }

                        FTP = true;
                    }


                }
                else
                {
                    FTP = false;
                }
                if (textBox2.Text.Trim() != string.Empty)
                {
                    if (SMode2.Set)
                    {
                        HRData2.ConvertRate(textBox2.Text);
                        HRPercentlist1 = HRData2.HRPercent;
                    }
                    HRData.ConvertRate(textBox2.Text);
                    HRPercentlist = HRData.HRPercent;

                    MaxHR = true;

                }
                else
                {
                    MaxHR = false;
                }


                clearDisplay();
                UpdateDisplay();
                UpdateDisplay2();
                comboBox2.Enabled = true;
            }
        }
    }
}
