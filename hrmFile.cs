﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZedGraph;

namespace PolarPal
{

    /// <summary>
    /// The hrmfile class navigates the hrm file submitted by the user for all of the relevant data. 
    /// One main attribute of this class is that it will determine which SMODEs are on and collect the data related to each SMODE.
    /// </summary>


    public class hrmFile
    {

       //Data lists for the HRM data.
        private List<double> hr = new List<double>();
        private List<double> speed = new List<double>();
        private List<double> cadence = new List<double>();
        private List<double> altitude = new List<double>();
        private List<double> power = new List<double>();
        private List<double> pPedalIndex = new List<double>();
        private List<int> IntervalList = new List<int>();
        public int dataLength = 0;

        //getters and setters for the lists.
        public List<double> Hr { get => hr; set => hr = value; }
        public List<double> Speed { get => speed; set => speed = value; }
        public List<double> Cadence { get => cadence; set => cadence = value; }
        public List<double> Altitude { get => altitude; set => altitude = value; }
        public List<double> Power { get => power; set => power = value; }
        public List<double> PPedalIndex { get => pPedalIndex; set => pPedalIndex = value; }
        public List<int> IntervalList1 { get => IntervalList; set => IntervalList = value; }


        /// <summary>
        /// The init method is the main method in the hrmfile class. This method will determine if the file being loaded is new or a comparison file and load the relevant methods for each file.
        /// </summary>
        /// <param name="pathToFile">String representation of the asbolute file path being loaded by the user.</param>
        /// <param name="SecondFile">A boolean to determine whether the second file is being loaded.</param>
        public void Init(string pathToFile, bool SecondFile)
        {
            //if second file load the second file methods.
            if (SecondFile)
            {
                SetParamsTwo(pathToFile);
                SetSModeTwo(Params2.SModeString, Params2.Version);
                SetHRMDataTwo(pathToFile);
                HRData2.Intervals = SetInterval(Params2.Interval).ToArray();
            }
            else // load the first file methods.
            {
                SetParams(pathToFile);
                SetSMode(Params.SModeString, Params.Version);
                SetHRMData(pathToFile);
                HRData.Intervals = SetInterval(Params.Interval).ToArray();
            }
            
            
           


        }

        /// <summary>
        /// This class will set the HRData for the session depending on which SMODE values are available.
        /// </summary>
        /// <param name="pathToFile"> the absolute path of the file must be sent as a parameter to load the file data.</param>
        private void SetHRMDataTwo(string pathToFile)
        {

            //clear the lists if they have been used already.
            hr.Clear();
            cadence.Clear();
            altitude.Clear();
            power.Clear();
            pPedalIndex.Clear();
            IntervalList.Clear();


            string text;
            using (StreamReader sr = new StreamReader(pathToFile)) //read the file.
            {
                while ((text = sr.ReadLine()) != null)
                {
                    if (text == "[HRData]") //if the line being read contains HRDATA -- meaning the HRDATA will be the next lines.
                    {
                        string val = "1";

                        while (val != null)
                        {
                            val = sr.ReadLine();
                            if (val != null)
                            {
                                if (SMode2.HRCC) //if smode hrcc is set to true meaning more than HR data was captured.
                                {
                                    string[] val_line = val.Split(separator: '\t');
                                    int i = 0;
                                    foreach (string value in val_line) //for each column in the data tsv.
                                    {
                                        double.TryParse(value, out double numval);

                                        // The following logic will take each cell of data and add it to it's corresponding SMODE value depending on the SMODE values being set.
                                        switch (i) //switch to each condition depending on which column the program is reading.
                                        {
                                            case 0:
                                                hr.Add(numval);
                                                break;
                                            case 1:
                                                if (SMode2.Speed)
                                                {
                                                    speed.Add(item: numval * 0.1);
                                                    break;

                                                }
                                                else if (SMode2.Cadence)
                                                {
                                                    Cadence.Add(numval);
                                                    break;

                                                }
                                                else if (SMode2.Altitude)
                                                {
                                                    Altitude.Add(numval);
                                                    break;

                                                }
                                                else if (SMode2.Power)
                                                {
                                                    Power.Add(numval);
                                                    break;

                                                }
                                                else if (SMode2.PPedalIndex)
                                                {
                                                    PPedalIndex.Add(numval);
                                                    break;

                                                }
                                                break;
                                            case 2:
                                                if (SMode2.Cadence)
                                                {
                                                    Cadence.Add(numval);
                                                    break;
                                                }
                                                else if (SMode2.Altitude)
                                                {
                                                    Altitude.Add(numval);
                                                    break;
                                                }
                                                else if (SMode2.Power)
                                                {
                                                    Power.Add(numval);
                                                    break;
                                                }
                                                else if (SMode2.PPedalIndex)
                                                {
                                                    PPedalIndex.Add(numval);
                                                    break;
                                                }
                                                break;
                                            case 3:
                                                if (SMode2.Altitude)
                                                {
                                                    Altitude.Add(numval);
                                                    break;
                                                }
                                                else if (SMode2.Power)
                                                {
                                                    Power.Add(numval);
                                                    break;
                                                }
                                                else if (SMode2.PPedalIndex)
                                                {
                                                    PPedalIndex.Add(numval);
                                                    break;
                                                }
                                                break;
                                            case 4:
                                                if (SMode2.Power)
                                                {
                                                    Power.Add(numval);
                                                    break;

                                                }
                                                else if (SMode2.PPedalIndex)
                                                {
                                                    PPedalIndex.Add(numval);
                                                    break;

                                                }
                                                break;
                                            case 5:
                                                if (SMode2.PPedalIndex)
                                                {
                                                    PPedalIndex.Add(numval);
                                                    break;

                                                }
                                                break;
                                            case 6:
                                                i = 0;
                                                break;
                                        }
                                        i++;
                                    }

                                }
                                else
                                {
                                    double.TryParse(val, out double numval);
                                    Hr.Add(numval);
                                }





                            }
                        }
                    }
                }
                //Set the HRDATA arrays as the new data arrays generated above.
                HRData2.HR = hr.ToArray();
                HRData2.Speed = speed.ToArray();
                HRData2.Cadence = cadence.ToArray();
                HRData2.Altitude = Altitude.ToArray();
                HRData2.Power = Power.ToArray();
                HRData2.PPedalIndex = PPedalIndex.ToArray();


            }
        }

        /// <summary>
        /// This class will set the smode values of the smode class depending on if they are available in the hrm file.
        /// </summary>
        /// <param name="sModeString"> the SMODE string from the hrm file.</param>
        /// <param name="version"> the version, this will determine the method of determination used for the SMODE columns</param>
        private void SetSModeTwo(string sModeString, string version)
        {
            char[] SMode_arr = sModeString.ToCharArray();

            if (version == "106")
            {
                //Speed True/False
                if (SMode_arr[0] == '1')
                {
                    SMode2.Speed = true;
                }
                else
                {
                    SMode2.Speed = false;
                }
                //Cadence True/False
                if (SMode_arr[1] == '1')
                {
                    SMode2.Cadence = true;
                }
                else
                {
                    SMode2.Cadence = false;
                }
                //Altitude True/False
                if (SMode_arr[2] == '1')
                {
                    SMode2.Altitude = true;
                }
                else
                {
                    SMode2.Altitude = false;
                }
                //Power True/False
                if (SMode_arr[3] == '1')
                {
                    SMode2.Power = true;
                }
                else
                {
                    SMode2.Power = false;
                }
                //PowerLeftRight True/False
                if (SMode_arr[4] == '1')
                {
                    SMode2.PowerLeftRight = true;
                }
                else
                {
                    SMode2.PowerLeftRight = false;
                }
                //Power Pedal Index True/False
                if (SMode_arr[5] == '1')
                {
                    SMode2.PPedalIndex = true;
                }
                else
                {
                    SMode2.PPedalIndex = false;
                }
                //HR/CC True/False
                if (SMode_arr[6] == '1')
                {
                    SMode2.HRCC = true;
                }
                else
                {
                    SMode2.HRCC = false;
                }
                //Set Smode as the smode of the first file...
                SMode2.UsEuro = SMode.UsEuro;
                SMode2.SpeedMetric = SMode.SpeedMetric;

                SMode2.Set = true;


            }
            else if (version == "107")
            {

            }
        }
        /// <summary>
        /// This method will set the Params class objects tot he parameters set in the hrm file.
        /// </summary>
        /// <param name="pathToFile"></param>
        private void SetParamsTwo(string pathToFile)
        {
            string text = "1";
            using (StreamReader sr = new StreamReader(pathToFile))
            {

                while ((text = sr.ReadLine()) != null)
                {
                    if (text == "[Params]")
                    {
                        string cal = "1";
                        while (cal != null)
                        {
                            cal = sr.ReadLine();
                            if (cal != null)
                            {
                                if (cal.Contains("Version"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.Version = vwords[1];


                                }
                                else if (cal.Contains("Monitor"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.Monitor = vwords[1];


                                }
                                else if (cal.Contains("SMode"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.SModeString = vwords[1];


                                }
                                else if (cal.Contains("Date"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.Date = vwords[1];


                                }
                                else if (cal.Contains("StartTime"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.StartTime = vwords[1];


                                }
                                else if (cal.Contains("Length"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.Length = vwords[1];

                                }
                                else if (cal.Contains("Interval"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.Interval = vwords[1];

                                }
                                else if (cal.Contains("Upper1"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.Upper1 = vwords[1];

                                }
                                else if (cal.Contains("Lower1"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.Lower1 = vwords[1];

                                }
                                else if (cal.Contains("Upper2"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.Upper2 = vwords[1];

                                }
                                else if (cal.Contains("Lower2"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.Lower2 = vwords[1];

                                }
                                else if (cal.Contains("Upper3"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.Upper3 = vwords[1];

                                }
                                else if (cal.Contains("Lower3"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.Lower3 = vwords[1];

                                }
                                else if (cal.Contains("Timer1"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.Timer1 = vwords[1];

                                }
                                else if (cal.Contains("Timer2"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.Timer2 = vwords[1];

                                }
                                else if (cal.Contains("Timer3"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.Timer3 = vwords[1];

                                }
                                else if (cal.Contains("ActiveLimit"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.ActiveLimit = vwords[1];

                                }
                                else if (cal.Contains("MaxHR"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.MaxHR = vwords[1];

                                }
                                else if (cal.Contains("RestHR"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.RestHR = vwords[1];

                                }
                                else if (cal.Contains("StartDelay"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.StartDelay = vwords[1];

                                }
                                else if (cal.Contains("VO2max"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.VO2max = vwords[1];

                                }
                                else if (cal.Contains("Weight"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params2.Weight = vwords[1];

                                }
                            }
                        }
                    }
                }
            }
        }


        /// <summary>
        /// This method will determine the interval length recorded in the hrm file and create a list of intervals used in the data graph of form1.
        /// </summary>
        /// <param name="interval"> The interval sent must be from the HRM file.</param>
        /// <returns></returns>
        public List<int> SetInterval(string interval)
        {
            
            int intval = 0;
            int val = 0;
            int.TryParse(interval, out intval);

            foreach (var i in Hr)
            {
                val = val + intval;

                IntervalList.Add(val);

                

            }

            return IntervalList;
        }

        /// <summary>
        /// This method will set the Params class objects tot he parameters set in the hrm file.
        /// </summary>
        /// <param name="pathToFile"></param>
        private void SetParams(string pathToFile)
        {
            string text = "1";
            using (StreamReader sr = new StreamReader(pathToFile))
            {

                while ((text = sr.ReadLine()) != null)
                {
                    if (text == "[Params]")
                    {
                        string cal = "1";
                        while (cal != null)
                        {
                            cal = sr.ReadLine();
                            if (cal != null)
                            {
                                if (cal.Contains("Version"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.Version = vwords[1];


                                }
                                else if (cal.Contains("Monitor"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.Monitor = vwords[1];


                                }
                                else if (cal.Contains("SMode"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.SModeString = vwords[1];


                                }
                                else if (cal.Contains("Date"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.Date = vwords[1];


                                }
                                else if (cal.Contains("StartTime"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.StartTime = vwords[1];


                                }
                                else if (cal.Contains("Length"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.Length = vwords[1];

                                }
                                else if (cal.Contains("Interval"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.Interval = vwords[1];

                                }
                                else if (cal.Contains("Upper1"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.Upper1 = vwords[1];

                                }
                                else if (cal.Contains("Lower1"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.Lower1 = vwords[1];

                                }
                                else if (cal.Contains("Upper2"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.Upper2 = vwords[1];

                                }
                                else if (cal.Contains("Lower2"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.Lower2 = vwords[1];

                                }
                                else if (cal.Contains("Upper3"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.Upper3 = vwords[1];

                                }
                                else if (cal.Contains("Lower3"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.Lower3 = vwords[1];

                                }
                                else if (cal.Contains("Timer1"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.Timer1 = vwords[1];

                                }
                                else if (cal.Contains("Timer2"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.Timer2 = vwords[1];

                                }
                                else if (cal.Contains("Timer3"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.Timer3 = vwords[1];

                                }
                                else if (cal.Contains("ActiveLimit"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.ActiveLimit = vwords[1];

                                }
                                else if (cal.Contains("MaxHR"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.MaxHR = vwords[1];

                                }
                                else if (cal.Contains("RestHR"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.RestHR = vwords[1];

                                }
                                else if (cal.Contains("StartDelay"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.StartDelay = vwords[1];

                                }
                                else if (cal.Contains("VO2max"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.VO2max = vwords[1];

                                }
                                else if (cal.Contains("Weight"))
                                {
                                    string[] vwords = cal.Split('=');
                                    Params.Weight = vwords[1];

                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// This class will set the HRData for the session depending on which SMODE values are available.
        /// </summary>
        /// <param name="pathToFile"> the absolute path of the file must be sent as a parameter to load the file data.</param>
        private void SetHRMData(string pathToFile)
        {
            string text;
            using (StreamReader sr = new StreamReader(pathToFile))
            {
                while ((text = sr.ReadLine()) != null)
                {
                    if (text == "[HRData]")
                    {
                        string val = "1";

                        while (val != null)
                        {
                            val = sr.ReadLine();
                            if (val != null)
                            {
                                if (SMode.HRCC)
                                {
                                    string[] val_line = val.Split(separator: '\t');
                                    int i = 0;
                                    foreach (string value in val_line)
                                    {
                                        double.TryParse(value, out double numval);


                                        switch (i)
                                        {
                                            case 0:
                                                hr.Add(numval);
                                                break;
                                            case 1:
                                                if (SMode.Speed)
                                                {
                                                    speed.Add(item: numval * 0.1);
                                                    break;

                                                }
                                                else if (SMode.Cadence)
                                                {
                                                    Cadence.Add(numval);
                                                    break;

                                                }
                                                else if (SMode.Altitude)
                                                {
                                                    Altitude.Add(numval);
                                                    break;

                                                }
                                                else if (SMode.Power)
                                                {
                                                    Power.Add(numval);
                                                    break;

                                                }
                                                else if (SMode.PPedalIndex)
                                                {
                                                    PPedalIndex.Add(numval);
                                                    break;

                                                }
                                                break;
                                            case 2:
                                                if (SMode.Cadence)
                                                {
                                                    Cadence.Add(numval);
                                                    break;
                                                }
                                                else if (SMode.Altitude)
                                                {
                                                    Altitude.Add(numval);
                                                    break;
                                                }
                                                else if (SMode.Power)
                                                {
                                                    Power.Add(numval);
                                                    break;
                                                }
                                                else if (SMode.PPedalIndex)
                                                {
                                                    PPedalIndex.Add(numval);
                                                    break;
                                                }
                                                break;
                                            case 3:
                                                if (SMode.Altitude)
                                                {
                                                    Altitude.Add(numval);
                                                    break;
                                                }
                                                else if (SMode.Power)
                                                {
                                                    Power.Add(numval);
                                                    break;
                                                }
                                                else if (SMode.PPedalIndex)
                                                {
                                                    PPedalIndex.Add(numval);
                                                    break;
                                                }
                                                break;
                                            case 4:
                                                if (SMode.Power)
                                                {
                                                    Power.Add(numval);
                                                    break;

                                                }
                                                else if (SMode.PPedalIndex)
                                                {
                                                    PPedalIndex.Add(numval);
                                                    break;

                                                }
                                                break;
                                            case 5:
                                                if (SMode.PPedalIndex)
                                                {
                                                    PPedalIndex.Add(numval);
                                                    break;

                                                }
                                                break;
                                            case 6:
                                                i = 0;
                                                break;
                                        }
                                        i++;
                                    }

                                }
                                else
                                {
                                    double.TryParse(val, out double numval);
                                    Hr.Add(numval);
                                }


                                


                            }
                        }
                    }
                }

                HRData.HR = hr.ToArray();
                HRData.Speed = speed.ToArray();
                HRData.Cadence = cadence.ToArray();
                HRData.Altitude = Altitude.ToArray();
                HRData.Power = Power.ToArray();
                HRData.PPedalIndex = PPedalIndex.ToArray();


            }

            
        }
        /// <summary>
        /// This class will set the smode values of the smode class depending on if they are available in the hrm file.
        /// </summary>
        /// <param name="sModeString"> the SMODE string from the hrm file.</param>
        /// <param name="version"> the version, this will determine the method of determination used for the SMODE columns</param>
        private void SetSMode(string SModeString, string Version)
        {
            char[] SMode_arr = SModeString.ToCharArray();

            if (Version == "106")
            {
                //Speed True/False
                if (SMode_arr[0] == '1')
                {
                    SMode.Speed = true;
                }
                else
                {
                    SMode.Speed = false;
                }
                //Cadence True/False
                if (SMode_arr[1] == '1')
                {
                    SMode.Cadence = true;
                }
                else
                {
                    SMode.Cadence = false;
                }
                //Altitude True/False
                if (SMode_arr[2] == '1')
                {
                    SMode.Altitude = true;
                }
                else
                {
                    SMode.Altitude = false;
                }
                //Power True/False
                if (SMode_arr[3] == '1')
                {
                    SMode.Power = true;
                }
                else
                {
                    SMode.Power = false;
                }
                //PowerLeftRight True/False
                if (SMode_arr[4] == '1')
                {
                    SMode.PowerLeftRight = true;
                }
                else
                {
                    SMode.PowerLeftRight = false;
                }
                //Power Pedal Index True/False
                if (SMode_arr[5] == '1')
                {
                    SMode.PPedalIndex = true;
                }
                else
                {
                    SMode.PPedalIndex = false;
                }
                //HR/CC True/False
                if (SMode_arr[6] == '1')
                {
                    SMode.HRCC = true;
                }
                else
                {
                    SMode.HRCC = false;
                }
                //Us/Euro True/False
                if (SMode_arr[7] == '1') // 1 = miles/ft -------- 0 = km/m
                {
                    SMode.UsEuro = true;
                    SMode.SpeedMetric = "Mph";
                }
                else
                {
                    SMode.SpeedMetric = "Kph";
                }

                SMode.Set = true;


            }
            else if (Version == "107")
            {

            }



        }
    }
}
