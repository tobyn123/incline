﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolarPal
{

    /// <summary>
    /// The SMode class sets and gets the various SMODE options.
    /// the objects inside can be used to determine the status of each polar setting.
    /// </summary>



    public static class SMode
    {

        //SMODE Values are either true or false. True for on and false for off.
        public static bool Speed { get;  set; }
        public static bool Cadence { get;  set; }
        public static bool Altitude { get;  set; }
        public static bool Power { get;  set; }
        public static bool PowerLeftRight { get;  set; }
        public static bool PPedalIndex { get;  set; }
        public static bool HRCC { get;  set; }
        public static bool UsEuro { get;  set; }
        public static string SpeedMetric { get;  set; }
        public static bool AirPressure { get;  set; }
        public static bool Set { get;  set; }


        
    }
}
